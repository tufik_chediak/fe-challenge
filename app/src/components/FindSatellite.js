import React, {Component} from 'react';
import {StyleSheet, Text, View, Image, ActivityIndicator} from "react-native";
import {CStyle, log, cuppa, Button, GetData} from "src/libs/cuppa/cuppa"
import {Style} from "../styles/Style";
import {Power2, TimelineMax} from "gsap";
import SetDirection from "./setDirection/SetDirection";
import {SCREEN} from "../Main";
import Satellite, {SATELLITE} from "../controller/Satellite";
require("src/libs/TweenMaxRN");

export default class FindSatellite extends Component {
    static defaultProps = { }

    constructor(props){
        super(props); cuppa.bindAll(this);
        this.state = {data:{name:""}};
    }

    componentDidMount(){
        let tl = new TimelineMax();
            tl.set(this.refs.btnNext, {style:{right:-200}});
            tl.set(this.refs.searching, {style:{opacity:0}});
            tl.set(this.refs.textFound, {style:{display:"none"}});
            tl.fromTo(this.refs.main, 0.6, {style:{opacity:0}}, {style:{opacity:1}, delay:0.5});
            tl.staggerFromTo([this.refs.sat1, this.refs.sat2, this.refs.sat3, this.refs.sat4], 0.4, {style:{opacity:0}}, {style:{opacity:0.5}}, 0.2);
            tl.add(this.findServer);
    }

    async findServer(){
        TweenMax.set(this.refs.searching, {style:{opacity:1}});
        await cuppa.wait(3000);
        Satellite.setSatellite({name:"SATTEFIUS-117", angle:190, altitude:30});
        let tl = new TimelineMax();
            tl.set(this.refs.searching, {style:{opacity:0}});
            tl.set(this.refs.textSearching, {style:{display:"none"}});
            tl.fromTo(this.refs.textFound, 0.4, {style:{opacity:0}}, {style:{opacity:1, display:"flex"},});
            tl.fromTo([this.refs.sat1, this.refs.sat3, this.refs.sat4], 0.4, {style:{opacity:0.5}}, {style:{opacity:0}}, 0.2);
            tl.to(this.refs.sat2, 0.4, {style:{opacity:1}}, "-=0.4");
            tl.to(this.refs.btnNext, 0.6, {style:{right:20}, delay:1, ease:Power2.easeOut} );
    }

    onPress(){
        cuppa.setData(SCREEN, {data:<SetDirection />})
    }

    componentWillUnmount(){ TweenMax.killAll(); }

    render() {
        return (
            <View ref={"main"} style={[CStyle.paddingX20, {flex:1, justifyContent:"center", alignItems:"center"}]}>
                <GetData name={SATELLITE} callback={(data)=>{ this.setState({data}) } }/>
                <Text style={[Style.color4, CStyle.marginY20, Style.fontOpenSandLight, {fontSize:40, textAlign: "center"}]}>Finding Your Sattelite</Text>
                <View style={{minHeight:80}}>
                    <Text ref={"textSearching"} style={[Style.color4, CStyle.t20, {textAlign: "center"}]}>We have lots of sattelites. We’re looking for a good match...</Text>
                    <Text ref={"textFound"} style={[Style.color4, CStyle.t20, {textAlign: "center"}]}>We’ve found the best satellite for you at your current location. Say ‘hello’ to your new best friend, {this.state.data.name}.</Text>
                </View>
                <View style={[CStyle.marginY20, {flex:1, width:"100%", justifyContent:"center", alignItems:"center", marginTop:40}]}>
                    <View style={[CStyle.cover, {top:90, left:90, bottom:90, right:90} ]}>
                        <Image source={require('src/media/images/world.png')} style={[{height:"100%", width:"100%", resizeMode:"contain" }]} fadeDuration={0} />
                    </View>
                    <Image ref={"sat1"} source={require('src/media/images/satellite.png')} style={[CStyle.absoluteLT,{height:90, width:90, resizeMode:"contain", opacity:0.4}]} fadeDuration={0} />
                    <Image ref={"sat2"} source={require('src/media/images/satellite.png')} style={[CStyle.absoluteRT,{height:90, width:90, resizeMode:"contain", opacity:0.4, transform: [{ rotate: '90deg'}]  }]} fadeDuration={0} />
                    <Image ref={"sat3"} source={require('src/media/images/satellite.png')} style={[CStyle.absoluteLB,{height:90, width:90, resizeMode:"contain", opacity:0.4, transform: [{ rotate: '270deg'}] }]} fadeDuration={0} />
                    <Image ref={"sat4"} source={require('src/media/images/satellite.png')} style={[CStyle.absoluteRB,{height:90, width:90, resizeMode:"contain", opacity:0.4, transform: [{ rotate: '180deg'}] }]} fadeDuration={0} />
                    <ActivityIndicator ref={"searching"} size={60} color="#DDD" style={[CStyle.absoluteCT, {marginLeft:-30} ]} />
                </View>
                <View ref={"btnNext"} style={[CStyle.positionRB, {right:20, bottom:20}]}>
                    <Button onPress={this.onPress} text={"Next"} style={[Style.button1]} background={Style.bg3.backgroundColor} backgroundPress={Style.bg3Dark.backgroundColor} textStyle={Style.button1Text}/>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({ })
