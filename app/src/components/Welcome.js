import React, {Component} from 'react';
import {StyleSheet, Text, View} from "react-native";
import {Button, CStyle, log, cuppa} from "src/libs/cuppa/cuppa"
import {Style} from "../styles/Style";
import FindSatellite from "./FindSatellite";
import {Power2, TimelineMax} from "gsap";
import {SCREEN} from "../Main";
require("src/libs/TweenMaxRN");

export default class Welcome extends Component {
    static defaultProps = { }

    constructor(props){
        super(props);
        this.state = {}
    }

    componentDidMount(){
        let tl = new TimelineMax();
            tl.set(this.refs.btnNext, {style:{right:-200}});
            tl.fromTo(this.refs.main, 0.6, {style:{opacity:0}}, {style:{opacity:1}, delay:0.5});
            tl.to(this.refs.btnNext, 0.6, {style:{right:20}, delay:0.5, ease:Power2.easeOut} );
    }

    onPress(){
        cuppa.setData(SCREEN, {data:<FindSatellite />})
    }

    render() {
        return (
            <View ref={"main"} style={[CStyle.paddingX20,{flex:1, justifyContent:"center", alignItems:"center", paddingBottom:70}]}>
                <Text style={[Style.color4, CStyle.marginY20, Style.fontOpenSandLight, {fontSize:50, textAlign:"center"}]}>SatteFi™</Text>
                <Text style={[Style.color4, CStyle.t20, {textAlign: "center"}]}>You’re just moments away from crystalclear phone calls and all-you-can-type messaging! Let’s get your antenna set up.</Text>
                <View ref={"btnNext"} style={[CStyle.positionRB, {right:20, bottom:20}]}>
                    <Button onPress={this.onPress} text={"Start"} style={[Style.button1]} background={Style.bg3.backgroundColor} backgroundPress={Style.bg3Dark.backgroundColor} textStyle={Style.button1Text}/>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({ })
