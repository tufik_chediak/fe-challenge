import React, {Component} from 'react';
import {StyleSheet, Text, View} from "react-native";
import {log, cuppa, CStyle, Button} from "src/libs/cuppa/cuppa"
import {Style} from "../styles/Style";
import {TimelineMax} from "gsap";
import {SCREEN} from "../Main";
import Welcome from "./Welcome";


export default class Finish extends Component {
    static defaultProps = { }

    constructor(props){
        super(props); cuppa.bindAll(this);
        this.state = {}
    }

    componentDidMount(){
        let tl = new TimelineMax();
            tl.fromTo(this.refs.main, 0.6, {style:{opacity:0}}, {style:{opacity:1}, delay:0.5});
    }

    return(){
        cuppa.setData(SCREEN, {data:<Welcome />})
    }

    render() {
        return (
            <View ref={"main"} style={[CStyle.paddingX20,{flex:1, justifyContent:"center", alignItems:"center", paddingTop:20, paddingBottom:40}]}>
                <Text style={[Style.color4, Style.fontOpenSandLight, {fontSize:44, textAlign:"center"}]}>Congratulations!</Text>
                <View style={{flex:1, justifyContent:"center"}}>
                    <Text style={[Style.color4,  {textAlign: "center", fontSize:26}]}>Now you know exactly where SATTEFI-US-117 lives. You should visit! But seriously, just point your antenna at the same place in the sky, and you’re all set. If you like, you can tap “FineTune” for some advanced adjustment options. Otherwise, welcome to SatteFi™.</Text>
                </View>
                <View style={{flexDirection:"row"}}>
                    <Button onPress={this.return} text={"Fine-tune"} style={[Style.button1, {marginRight:5}]} background={Style.bg1.backgroundColor} backgroundPress={Style.bg1Dark.backgroundColor} textStyle={[Style.button1Text, {color:Style.color3.color}]}/>
                    <Button onPress={()=>{ alert("Press Fine-tune to start again.") }}  text={"Finish"} style={[Style.button1, {marginLeft:5}]} background={Style.bg3.backgroundColor} backgroundPress={Style.bg3Dark.backgroundColor} textStyle={Style.button1Text}/>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({ })
