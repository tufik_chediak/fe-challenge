import React, {Component} from 'react';
import {Image, StyleSheet, Text, View} from "react-native";
import {log, cuppa, CStyle, Button, GetData} from "src/libs/cuppa/cuppa"
import {Style} from "../../styles/Style";
import {Power2, TimelineMax} from "gsap";
import { magnetometer, setUpdateIntervalForType, SensorTypes } from "react-native-sensors";
import {SATELLITE} from "../../controller/Satellite";
import {SCREEN} from "../../Main";
import VerticalPosition from "../verticalPosition/VerticalPosition";
require("src/libs/TweenMaxRN");

setUpdateIntervalForType(SensorTypes.magnetometer, 100);

export default class Calibrate extends Component {
    static defaultProps = { }
    subscription = null;

    constructor(props){
        super(props); cuppa.bindAll(this);
        this.state = {data:{name:"Test", angle:190, altitude:30}, userAngle:0};
    }

    componentDidMount(){
        let tl = new TimelineMax();
            tl.set(this.refs.btnNext, {style:{right:-200}});
            tl.fromTo(this.refs.main, 0.6, {style:{opacity:0}}, {style:{opacity:1}});
        this.subscription = magnetometer.subscribe(this.onUpdate);
    }

    onUpdate(data){
        let angle = 0;
        if (data) {
            let {x, y, z} = data;
            if (Math.atan2(y, x) >= 0) { angle = Math.atan2(y, x) * (180 / Math.PI); }
            else { angle = (Math.atan2(y, x) + 2 * Math.PI) * (180 / Math.PI); }
        }
        angle = Math.round(angle);
        let userAngle = angle - (this.state.data.angle+117);
        let percent = 1-Math.abs(userAngle/180);
        this.setState({userAngle:userAngle});
        TweenMax.set(this.refs.hours, { transform: { rotate: userAngle+'deg' }  });
        TweenMax.set(this.refs.clockFrame, { style:{opacity:1*percent} });
        if(percent > 0.93){
            TweenMax.set(this.refs.txtPerfect, { style:{opacity:1} });
            TweenMax.killTweensOf(this.refs.btnNext);
            TweenMax.to(this.refs.btnNext, 0.6, {style:{right:0}, ease:Power2.easeOut} );
        }else{
            TweenMax.set(this.refs.txtPerfect, { style:{opacity:0} });
            TweenMax.killTweensOf(this.refs.btnNext);
            TweenMax.to(this.refs.btnNext, 0.6, {style:{right:-200}} );
        }
    }

    onPress(){
        cuppa.setData(SCREEN, {data:<VerticalPosition/>})
    }

    componentWillUnmount(){
        TweenMax.killAll();
        if(this.subscription) this.subscription.unsubscribe();
    }

    render() {
        return (
            <View ref={"main"} style={[{flex:1, width:"100%", alignItems:"center", marginTop:0, paddingBottom:90}]}>
                <GetData name={SATELLITE} callback={(data)=>{ this.setState({data}) } }/>
                <Text style={[Style.color4, CStyle.t20, {textAlign: "center", fontSize:24, maxWidth:250}]}>The <Text style={{fontWeight:"bold"}}>minute hand</Text> is where you’re pointing</Text>
                <View style={[{flex:1, justifyContent:"center", alignItems:"center"}]}>
                    <View style={[{width:260, height:260}]}>
                        <Image ref={"minutes"} source={require('src/media/images/clock_minutes.png')} style={[CStyle.absoluteLT, {height:260, width:260, resizeMode:"contain" }]} fadeDuration={0} />
                        <Image ref={"hours"} source={require('src/media/images/clock_hours.png')} style={[CStyle.absoluteLT, Style.tint3, {height:260, width:260, resizeMode:"contain" }]} fadeDuration={0} />
                        <Image source={require('src/media/images/clock_frame.png')} style={[CStyle.absoluteLT, {height:260, width:260, resizeMode:"contain" }]} fadeDuration={0} />
                        <Image ref={"clockFrame"} source={require('src/media/images/clock_frame.png')} style={[CStyle.absoluteLT, Style.tint3, {height:260, width:260, resizeMode:"contain", opacity:0}]} fadeDuration={0} />
                        <Text ref={"txtPerfect"} style={[CStyle.cover, Style.color3, {top:"auto", bottom:40, fontSize:30, textAlign:"center", fontWeight:"bold"}]}>Perfect</Text>
                    </View>
                </View>
                <Text style={[Style.color4, {textAlign: "center", fontSize:24, maxWidth:250, paddingBottom:20}]}>The <Text style={[Style.color3, {fontWeight:"bold"}]}>hour hand</Text> is {this.state.data.name}</Text>
                <View ref={"btnNext"} style={[CStyle.positionRB, {right:0, bottom:20}]}>
                    <Button onPress={this.onPress} text={"Next"} style={[Style.button1]} background={Style.bg3.backgroundColor} backgroundPress={Style.bg3Dark.backgroundColor} textStyle={Style.button1Text}/>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({ })
