import React, {Component} from 'react';
import {StyleSheet, Text, View} from "react-native";
import {log, cuppa, CStyle, GetData} from "src/libs/cuppa/cuppa"
import {Style} from "../../styles/Style";
import Instructions from "./Instructions";
import {TimelineMax} from "gsap";
require("src/libs/TweenMaxRN");

export const DIRECTION_SCREEN = "DIRECTION_SCREEN";
export default class SetDirection extends Component {
    static defaultProps = { }

    constructor(props){
        super(props); cuppa.bindAll(this);
        this.state = {screen:null}
        cuppa.setData(DIRECTION_SCREEN, {data:<Instructions />});
    }

    componentDidMount(){
        let tl = new TimelineMax();
            tl.fromTo(this.refs.main, 0.6, {style:{opacity:0}}, {style:{opacity:1}, delay:0.5});
    }

    render() {
        return (
            <View  ref={"main"}  style={[CStyle.paddingX20, {flex:1, justifyContent:"center", alignItems:"center"}]}>
                <GetData name={DIRECTION_SCREEN} callback={(screen)=>{ this.setState({screen}) } }/>
                <Text style={[Style.color4, CStyle.marginY20, Style.fontOpenSandLight, {fontSize:40, textAlign: "center"}]}>Pointing in the right direction</Text>
                {this.state.screen}
            </View>
        );
    }
}

const styles = StyleSheet.create({ })
