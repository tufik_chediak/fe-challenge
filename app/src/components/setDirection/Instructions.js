import React, {Component} from 'react';
import {Image, StyleSheet, Text, View} from "react-native";
import {log, cuppa, CStyle, Button} from "src/libs/cuppa/cuppa"
import {Style} from "../../styles/Style";
import {Power2, TimelineMax} from "gsap";
import {DIRECTION_SCREEN} from "./SetDirection";
import Calibrate from "./Calibrate";
require("src/libs/TweenMaxRN");

export default class Instructions extends Component {
    static defaultProps = { }

    constructor(props){
        super(props); cuppa.bindAll(this);
        this.state = {}
    }

    componentDidMount(){
        let tl = new TimelineMax();
            tl.set(this.refs.btnNext, {style:{right:-200}});
            tl.to(this.refs.btnNext, 0.6, {style:{right:0}, delay:1, ease:Power2.easeOut} );
    }

    onPress(){
        cuppa.setData(DIRECTION_SCREEN, {data:<Calibrate/>})
    }

    render() {
        return (
            <View style={[{flex:1, justifyContent:"center", alignItems:"center", paddingBottom:80}]}>
                <Text ref={"textSearching"} style={[Style.color4, {textAlign: "center", fontSize:26, marginTop:40}]}>Stand with your phone in your hand, arm extended, screen facing up, and rotate on your feet until the two clock hands on the next screen are at midnight.</Text>
                <Image source={require('src/media/images/clock.png')} style={[{height:156, width:156, resizeMode:"contain", marginTop:40 }]} fadeDuration={0} />
                <View ref={"btnNext"} style={[CStyle.positionRB, {right:0, bottom:20}]}>
                    <Button onPress={this.onPress} text={"Next"} style={[Style.button1]} background={Style.bg3.backgroundColor} backgroundPress={Style.bg3Dark.backgroundColor} textStyle={Style.button1Text}/>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({ })
