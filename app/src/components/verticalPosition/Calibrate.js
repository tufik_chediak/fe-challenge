import React, {Component} from 'react';
import {Image, StyleSheet, Text, View} from "react-native";
import {log, cuppa, CStyle, Button, GetData} from "src/libs/cuppa/cuppa"
import {Style} from "../../styles/Style";
import {Power2, TimelineMax} from "gsap";
import {accelerometer, setUpdateIntervalForType, SensorTypes} from "react-native-sensors";
import {SCREEN} from "../../Main";
import Finish from "../Finish";
import {SATELLITE} from "../../controller/Satellite";
require("src/libs/TweenMaxRN");

setUpdateIntervalForType(SensorTypes.accelerometer, 100);

export default class Calibrate extends Component {
    static defaultProps = { }
    subscription = null;

    constructor(props){
        super(props); cuppa.bindAll(this);
        this.state = {data:{name:"Test", angle:190, altitude:30}};
    }

    componentDidMount(){
        let tl = new TimelineMax();
            tl.set(this.refs.btnNext, {style:{right:-200}});
            tl.fromTo(this.refs.main, 0.6, {style:{opacity:0}}, {style:{opacity:1}});
        this.subscription = accelerometer.subscribe(this.onUpdate);
    }

    onUpdate(data){
        let {x, y, z} = data;
        let percent = cuppa.percent(z, 0, 10);
        if(y < 0) percent = 1;
        if(percent < 0) percent = 0;
        if(percent > 1) percent = 1;
        TweenMax.set([this.refs.mustache, this.refs.mustache2], { style:{top:(percent*78)+"%"} });
        if(percent > 0.80 && percent < 0.90 ){
            TweenMax.set(this.refs.mustache2, { style:{opacity:1} });
            TweenMax.set(this.refs.txtPerfect, { style:{opacity:1} });
            TweenMax.to(this.refs.btnNext, 0.6, {style:{right:0}, ease:Power2.easeOut} );
        }else{
            TweenMax.set(this.refs.mustache2, { style:{opacity:0} });
            TweenMax.set(this.refs.txtPerfect, { style:{opacity:0} });
            TweenMax.to(this.refs.btnNext, 0.6, {style:{right:-200}} );
        }
    }

    onPress(){
        cuppa.setData(SCREEN, {data:<Finish />})
    }

    componentWillUnmount(){
        TweenMax.killAll();
        if(this.subscription) this.subscription.unsubscribe();
    }

    render() {
        return (
            <View ref={"main"} style={[{flex:1, width:"100%", alignItems:"center", marginTop:0, paddingBottom:90}]}>
                <GetData name={SATELLITE} callback={(data)=>{ log("SATELLITE", data); this.setState({data}) } }/>
                <Text style={[Style.color4, CStyle.t20, {textAlign: "center", fontSize:24, maxWidth:250}]}>The <Text style={{fontWeight:"bold"}}>minute hand</Text> is where you’re pointing</Text>
                <View style={[{flex:1, marginTop:20, width:"100%", marginBottom:40}]}>
                    <Image source={require('src/media/images/hipster.png')} style={[CStyle.absoluteLT, Style.tint3, {width:"100%", height:"100%", resizeMode:"contain" }]} fadeDuration={0} />
                    <Image ref={"mustache"} source={require('src/media/images/mustache.png')} style={[CStyle.absoluteLT, {width:"100%", height:40, resizeMode:"contain" }]} fadeDuration={0} />
                    <Image ref={"mustache2"} source={require('src/media/images/mustache.png')} style={[CStyle.absoluteLT, Style.tint3, {width:"100%", height:40, opacity:0, resizeMode:"contain" }]} fadeDuration={0} />
                </View>
                <Image ref={"txtPerfect"} source={require('src/media/images/100.png')} style={[Style.tint5, CStyle.absoluteLB, {width:"100%", height:60, opacity:0, resizeMode:"contain", bottom:40 }]} fadeDuration={0} />
                <View ref={"btnNext"} style={[CStyle.positionRB, {right:0, bottom:20}]}>
                    <Button onPress={this.onPress} text={"Next"} style={[Style.button1]} background={Style.bg3.backgroundColor} backgroundPress={Style.bg3Dark.backgroundColor} textStyle={Style.button1Text}/>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({ })
