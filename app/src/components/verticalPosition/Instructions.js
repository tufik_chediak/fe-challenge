import React, {Component} from 'react';
import {StyleSheet, Text, View} from "react-native";
import {log, cuppa, CStyle, Button} from "src/libs/cuppa/cuppa"
import {Style} from "../../styles/Style";
import {Power2, TimelineMax} from "gsap";
import {VERTICAL_SCREEN} from "./VerticalPosition";
import Calibrate from "./Calibrate";
require("src/libs/TweenMaxRN");

export default class Instructions extends Component {
    static defaultProps = { }

    constructor(props){
        super(props); cuppa.bindAll(this);
        this.state = {}
    }

    componentDidMount(){
        let tl = new TimelineMax();
            tl.set(this.refs.btnNext, {style:{right:-200}});
            tl.to(this.refs.btnNext, 0.6, {style:{right:0}, delay:1, ease:Power2.easeOut} );
    }

    onPress(){
        cuppa.setData(VERTICAL_SCREEN, {data:<Calibrate />})
    }

    render() {
        return (
            <View style={[{flex:1, alignItems:"center", justifyContent:"center", marginTop:0, paddingBottom:70}]}>
                <Text ref={"textSearching"} style={[Style.color4, {textAlign: "center", fontSize:26}]}>Now we ask, “How high?” You need to point about 30º up from the ground. We don’t know what that means, either, but with your arm out, like before, raise and lower it until the handlebar mustache fits.</Text>
                <View ref={"btnNext"} style={[CStyle.positionRB, {right:0, bottom:20}]}>
                    <Button onPress={this.onPress} text={"Next"} style={[Style.button1]} background={Style.bg3.backgroundColor} backgroundPress={Style.bg3Dark.backgroundColor} textStyle={Style.button1Text}/>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({ })
