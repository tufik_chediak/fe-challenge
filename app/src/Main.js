import React, {Component} from 'react';
import {View} from "react-native";
import {log, StatusBarComp, cuppa} from "src/libs/cuppa/cuppa";
import Welcome from "./components/Welcome";
import {Style} from "./styles/Style";

export const SCREEN = 'SCREEN';
export default class Main extends Component {
    static defaultProps = { }

    constructor(props){
        super(props); cuppa.bindAll(this);
        this.state = {screen:<Welcome />}
    }

    componentDidMount(){
        cuppa.getData(SCREEN, {callback:(screen)=>{ this.setState({screen:screen}) } });
    }

    render() {
        return (
            <View style={{backgroundColor:Style.bg2.backgroundColor, flex:1}}>
                <StatusBarComp color={"light-content"} />
                { this.state.screen }
            </View>
        );
    }
}
