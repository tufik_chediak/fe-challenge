import React, {Component} from 'react';
import {StyleSheet, Text, View} from "react-native";
import {log, StatusBarComp, cuppa} from "src/libs/cuppa/cuppa"


export default class Template extends Component {
    static defaultProps = { }

    constructor(props){
        super(props); cuppa.bindAll(this);
        this.state = {}
    }

    componentDidMount(){ }
    componentWillUnmount(){ }
    componentWillUpdate(nextProps, nextState) { }
    componentDidUpdate(prevProps, prevState) { }

    render() {
        return (
            <View>
                <StatusBarComp />
                <Text>Hello There!</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({ })
