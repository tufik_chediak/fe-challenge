import {StyleSheet} from "react-native";

export const Style = StyleSheet.create({
    bg0:{ backgroundColor: "#000" }, color0:{ color: "#000" }, tint0:{ tintColor: "#000" },
    bg1:{ backgroundColor: "#FFF" }, color1:{ color: "#FFF" }, tint1:{ tintColor: "#FFF" },
    bg1Dark:{ backgroundColor: "#CCC" }, color1Dark:{ color: "#CCC" }, tint1Dark:{ tintColor: "#CCC" },
    bg2:{ backgroundColor: "#4A90E2" }, color2:{ color: "#4A90E2" }, tint2:{ tintColor: "#4A90E2" },
    bg3:{ backgroundColor: "#9013FE" }, color3:{ color: "#9013FE" }, tint3:{ tintColor: "#9013FE" },
    bg3Dark:{ backgroundColor: "#5c00ad" }, color3Dark:{ color: "#5c00ad" }, tint3Dark:{ tintColor: "#5c00ad" },
    bg4:{ backgroundColor: "#E9E9E9" }, color4:{ color: "#E9E9E9" }, tint4:{ tintColor: "#E9E9E9" },
    bg5:{ backgroundColor: "#E10000" }, color5:{ color: "#E10000" }, tint5:{ tintColor: "#E10000" },
    textInput:{ fontSize: 16, color:"#444", borderWidth:0, borderStyle:"solid", borderBottomWidth: 1, borderColor:"#C8C8C8", padding:0, paddingLeft:15, paddingRight:15, backgroundColor:"transparent", width:"100%", height:43 },
    textArea:{ textAlignVertical:"top", height:"auto", minHeight:100, marginTop:10, paddingLeft:15, paddingRight:15 },
    label:{color:"#777777", fontSize:14,},
    textInput2: {fontSize: 16, color: '#333', borderWidth: 0, padding: 0, backgroundColor: '#F5F5F5', width: '100%', height: 43, paddingLeft: 15, paddingRight: 15, },
    button1:{paddingLeft:20, paddingRight:20, borderRadius:40, elevation: 4, height:55},
    button1Text:{fontSize:30, color:"#FFF", fontFamily:"sans-serif-light"},
    fontOpenSandLight:{ fontFamily:"sans-serif-light" },
});
