/* INSTRUCTIONS
npm install gsap
npm install --save react-native-status-bar-height
npm install --save prop-types
npm install yarn
*/
import React, { Component } from 'react';
import { Text, View, Image, StyleSheet, TouchableWithoutFeedback, ScrollView, Dimensions, Platform, ActivityIndicator, StatusBar, AsyncStorage, BackHandler, PanResponder} from 'react-native';
import PropTypes from "prop-types";
import { getStatusBarHeight } from 'react-native-status-bar-height';
import {Expo, Power0, Power2, TimelineMax, TweenMax} from "gsap";
import {SATELLITE} from "../../controller/Satellite";
require("../TweenMaxRN");

export var log = function(values){ try{ let args = arguments; for(let i = 0; i < args.length; i++){ console.log(args[i]); } }catch(err){ }; };
export var cuppa = { };

/* bindAll */
    cuppa.bindAll = function(element){
        var propertyNames = Object.getOwnPropertyNames(Object.getPrototypeOf(element));
        for(var i = 0; i < propertyNames.length; i++){
            if( ["constructor"].indexOf(propertyNames[i]) == -1 ){
                if(typeof element[propertyNames[i]] == "function"){
                    element[propertyNames[i]]= element[propertyNames[i]].bind(element);
                };
            };
        };
    };

/* Unique value */
    cuppa.unique = function(add_to_init){
        var value = Math.round(Math.random()*9999) + new Date().valueOf();
        if(add_to_init) value = add_to_init + value;
        return value;
    };

/* Short Object ASC or DESC */
    cuppa.shortObject = function(object, reverse){
        var keys = Object.keys(object);
        keys.sort();
        if(reverse) keys.reverse();
        var tmpObject = {}
        for(var i = 0; i < keys.length; i++){
            tmpObject[keys[i]] = object[keys[i]];
        };
        return tmpObject;
    };

/* sDim, scale Dimention acording to the device screeen
    // specify the layout dimentions of your design
        global.layoutWidth = 320; global.layoutHeight = 569;
    // Use
        cuppa.sDim(200, 40)   // return the new dimention according the device screen
*/
    global.layoutWidth = 320; global.layoutHeight = 569;
    cuppa.sDim = function(width, height, force){
        if(force == undefined) force = "w";
        let dimWindow = Dimensions.get('window');
        if(dimWindow.width > dimWindow.height){
            let tW = dimWindow.width;
            dimWindow.width = dimWindow.height;
            dimWindow.height = tW;
        }
        let percent = 1;
        if(force == "w" || force == "width") percent = dimWindow.width/global.layoutWidth;
        else if(force == "h" || force == "height") percent = dimWindow.height/global.layoutHeight;
        let dim = {width:width*percent, height:height*percent, percent:percent};
        return dim;
    }
/* timeout */
    cuppa.timeout = async function(time){
        if(!time) time = 1000;
        let result = await new Promise((resolve)=>{ setTimeout(()=>{ resolve(true) }, time); });
        return result;
    };
    cuppa.wait = async function(time){ return await cuppa.timeout(time); }

/* getData */
    export class GetData extends Component {
        static propTypes = {callback:PropTypes.func, name:PropTypes.string};
        static defaultProps = {callback:null, name:"default"}

        constructor(props){
            super(props); cuppa.bindAll(this);
            this.state = {}
        }

        componentDidMount(){
            cuppa.getData(this.props.name, {callback:this.getData});
        }

        getData(data){
            if(this.props.callback) this.props.callback(data);
        }

        componentWillUnmount(){
            log("componentWillUnmount");
            cuppa.removeListener(this.props.name, this.getData);
        }

        render() {
            return (null);
        }
    }

/* Button
    Example
        <Button text="Gallery" textStyle={{textAlign:"left"}} />
        <Button onPress={ ()=>{ }} background={"#FFF"} backgroundPress={"#DDD"} style={{height:"100%", width:"50%"}} >
            <Image source={require("src/media/images/icon-photo.png")} style={{height:15}} resizeMode={"contain"} />
            <Text style={{fontSize:11}} >Graphic Note</Text>
        </Button>
*/
    export class Button extends Component{
        static propTypes = {background:PropTypes.string, backgroundPress:PropTypes.string, disabled:PropTypes.bool, disabledOpacity:PropTypes.number, onPress:PropTypes.func, text:PropTypes.string, textStyle:PropTypes.any, style:PropTypes.any, selected:PropTypes.bool};
        static defaultProps = {background: "#eee", backgroundPress: "#ddd", disabled: false, disabledOpacity:0.6, onPress: null, text: null, textStyle: {}, selected:false,};

        constructor(props) {
            super(props);
            this.state = {};
            this.childrens = props.children;
            if (!Array.isArray(this.childrens)) this.childrens = [props.children];
        }

        componentDidMount() {
            if(this.props.selected) this.onPressIn();
        }

        componentWillUnmount() {
            TweenMax.killTweensOf(this.refs.view);
        }

        onPressIn = function (e) {
            TweenMax.fromTo(this.refs.view, 0.3, {style: {"backgroundColor": this.props.background}}, {style: {"backgroundColor": this.props.backgroundPress}});
        }.bind(this)

        onPressOut = function (e) {
            TweenMax.to(this.refs.view, 0.3, {style: {"backgroundColor": this.props.background}});
        }.bind(this)

        selected(value = true){
            if(value) this.onPressIn();
            else this.onPressOut();
        }

        render() {
            let opacity = (this.props.disabled) ? this.props.disabledOpacity : 1;
            return (
                <TouchableWithoutFeedback disabled={this.props.disabled} onPressIn={this.onPressIn} onPressOut={this.onPressOut} onPress={this.props.onPress}>
                    <View ref="view" style={[{backgroundColor: this.props.background, height: 43, alignItems: 'center', justifyContent: 'center', paddingLeft: 10, paddingRight: 10, opacity:opacity}, this.props.style]}>
                        {
                            this.childrens.map((element, key) => {
                                return (element)
                            })
                        }
                        {(this.props.text) ? <Text style={[{fontSize: 14, color: "#444", width:"100%", textAlign:"center"}, this.props.textStyle]}>{this.props.text}</Text> : null}
                    </View>
                </TouchableWithoutFeedback>
            )
        }
    }

/* CheckBox
    Example
    <CheckBox imageChecked={require("src/media/images/checkbox-checked.png")} imageUnchecked={require("src/media/images/checkbox-unchecked.png")} />
*/
    export class CheckBox extends Component {
        static propTypes = {style:PropTypes.any, checked:PropTypes.bool, disabled:PropTypes.bool, onPress:PropTypes.func, imageChecked: PropTypes.string, imageUnchecked: PropTypes.string};
        static defaultProps = { style:{}, checked:false, disabled:false, onPress: null, imageChecked:null, imageUnchecked:null };

        constructor(props) {
            super(props);
            this.state = {checked:false};
        }

        componentDidMount(){
            this.checked(this.props.checked, 0);
        }

        checked = function(value, duration = 0.3){
            if(value == undefined) value = !this.state.checked;
            this.setState({checked:value});

            if(value)
                TweenMax.to(this.refs.imgChecked, duration, {style: {alpha: 1} } );
            else
                TweenMax.to(this.refs.imgChecked, duration, {style: {alpha: 0} } );

            if(this.props.onPress) this.props.onPress(this.state.checked);
        }.bind(this)

        render() {
            return (
                <TouchableWithoutFeedback disabled={this.props.disabled} onPress={()=>{ this.checked(); }} >
                    <View style={[{width:21, height:21},this.props.style]}>
                        <Image ref={"imgUncheck"} source={this.props.imageUnchecked} style={[CStyle.cover, {height:"100%", width:"100%", opacity:1}] } resizeMode={"contain"} />
                        <Image ref={"imgChecked"} source={this.props.imageChecked} style={[CStyle.cover, {height:"100%", width:"100%", opacity:0}] } resizeMode={"contain"} />
                    </View>
                </TouchableWithoutFeedback>
            )
        }
    }

/* Progress */
    export class ProgressIndicator extends Component{
        static propTypes = {color:PropTypes.string, size:PropTypes.number, style:PropTypes.any, onClose:PropTypes.func, text:PropTypes.string, textStyle:PropTypes.any};
        static defaultProps =   { color:"#777", size:40, style:{}, onClose:null, text:"", textStyle: {} };
        constructor(props) {
            super(props);
            this.state = {text:this.props.text}
        }
        componentDidMount(){
            cuppa.popShow({ref:this.refs.main, blockade:this.refs.blockade});
        }

        setText(text){
            this.props.text = text;
            this.setState({text:text});
        }

        close(){
            cuppa.popHide({ref:this.refs.main, blockade:this.refs.blockade, callback:this.props.onClose});
        }

        render() {
            return (
                <View style={[CStyle.cover, {justifyContent:"center", alignItems:"center", zIndex:9999, elevation:9999}]}>
                    <View ref={"blockade"} style={[CStyle.blockade]}></View>
                    <View ref={"main"} style={[{padding:20, backgroundColor:"#FFF", borderRadius:5, overflow:"hidden"}, this.props.style]}>
                        <ActivityIndicator size={this.props.size} color={this.props.color} />
                        {
                            (this.state.text) ? (
                                <View style={{alignItems:"center"}}>
                                    <Text style={[this.props.textStyle]}>{this.state.text}</Text>
                                </View>
                            ) : (null)
                        }
                    </View>
                </View>
            )
        }
    }

/* NavBar */
    export class NavBar extends Component{
        static OPEN = "OPEN";
        static CLOSE = "CLOSE";
        static LEFT = "LEFT";
        static RIGHT = "RIGHT";
        static propTypes ={open:PropTypes.bool, style:PropTypes.any, styleNavBar:PropTypes.any, easeOpen:PropTypes.any, easeClose:PropTypes.any, width:PropTypes.number, content:PropTypes.any, position:PropTypes.oneOf(['LEFT', 'RIGHT'])};
        static defaultProps = {open:false, style:null, styleNavBar:null, easeOpen:Expo.easeOut, easeClose:Expo.easeOut, width:300, content:null, status:NavBar.OPEN, position:NavBar.LEFT};
        swipeConfig = {velocityThreshold: 0.3, directionalOffsetThreshold: 80, gestureIsClickThreshold: 5};

        constructor(props){
            super(props); cuppa.bindAll(this);
            this.state = {};
            this.panResponder = PanResponder.create({
                onMoveShouldSetPanResponder: (evt, state) => { return !this.isClick(state); },
                onPanResponderTerminationRequest: (evt, state) => true,
                onPanResponderMove: this.onMove,
                onPanResponderRelease:this.onRelease,
            });
        }

        componentDidMount(){
            if(!this.props.open){
                TweenMax.set(this.refs.root,{style:{display:"none", left:9999}});
                this.setState({status:NavBar.CLOSE});
                BackHandler.removeEventListener('hardwareBackPress', this.onBackHandler);
            }
        }

        open(duration = 0.4){
            BackHandler.addEventListener('hardwareBackPress', this.onBackHandler);
            if(this.props.position == NavBar.LEFT) {
                if (this.ani) { this.ani.kill(); }
                else {
                    TweenMax.set(this.refs.menu, {style: {left: -this.props.width}});
                    TweenMax.set(this.refs.blockade, {style: {opacity: 0}});
                    TweenMax.set(this, {state: {status: NavBar.OPEN}});
                }
                TweenMax.set(this.refs.root, {style: {left: 0, display: "flex"}});
                this.ani = new TimelineMax();
                this.ani.to(this.refs.blockade, duration, {style: {opacity: 1}}, 0);
                this.ani.to(this.refs.menu, duration, {style: {left: 0}, ease: this.props.easeOpen}, 0);
            }else{
                if (this.ani) { this.ani.kill(); }
                else {
                    TweenMax.set(this.refs.menu, {style: {right: -this.props.width}});
                    TweenMax.set(this.refs.blockade, {style: {opacity: 0}});
                    TweenMax.set(this, {state: {status: NavBar.OPEN}});
                }
                TweenMax.set(this.refs.root, {style: {left: 0, display: "flex"}});
                this.ani = new TimelineMax();
                this.ani.to(this.refs.blockade, duration, {style: {opacity: 1}}, 0);
                this.ani.to(this.refs.menu, duration, {style: {right: 0}, ease: this.props.easeOpen}, 0);
            }
        }

        close(duration = 0.3){
            if(this.props.position == NavBar.LEFT) {
                if (this.ani) { this.ani.kill();}
                else {
                    TweenMax.set(this.refs.menu, {style: {left: 0}});
                    TweenMax.set(this.refs.blockade, {style: {opacity: 1}});
                }
                TweenMax.set(this.refs.root, {style: {left: 0, display: "flex"}});
                this.ani = new TimelineMax();
                this.ani.to(this.refs.blockade, duration, {style: {opacity: 0}}, 0);
                this.ani.to(this.refs.menu, duration, { style: {left: -this.props.width}, ease: this.props.easeClose}, 0);
                this.ani.set(this.refs.root, {style: {display: "none", left: 9999}});
                this.ani.set(this, {state: {status: NavBar.CLOSE}});
                this.ani.add(() => {
                    BackHandler.removeEventListener('hardwareBackPress', this.onBackHandler);
                });
            }else{
                if (this.ani) { this.ani.kill();}
                else {
                    TweenMax.set(this.refs.menu, {style: {right: 0}});
                    TweenMax.set(this.refs.blockade, {style: {opacity: 1}});
                }
                TweenMax.set(this.refs.root, {style: {left: 0, display: "flex"}});
                this.ani = new TimelineMax();
                this.ani.to(this.refs.blockade, duration, {style: {opacity: 0}}, 0);
                this.ani.to(this.refs.menu, duration, { style: {right: -this.props.width}, ease: this.props.easeClose}, 0);
                this.ani.set(this.refs.root, {style: {display: "none", left: 9999}});
                this.ani.set(this, {state: {status: NavBar.CLOSE}});
                this.ani.add(() => {
                    BackHandler.removeEventListener('hardwareBackPress', this.onBackHandler);
                });
            }
        }

        onBackHandler(){ this.close(); return true; }

        onMove(e, state){
            if(this.props.position == NavBar.LEFT){
                let posX = state.dx;
                if(posX > 0) posX = 0;
                let percent = 1 - Math.abs(posX/this.props.width);
                this.ani = TweenMax.set(this.refs.menu, {style:{left:posX}});
                TweenMax.set(this.refs.blockade, {style:{opacity:percent}});
            }else{
                let posX = state.dx*-1;
                if(posX > 0) posX = 0;
                let percent = 1 - Math.abs(posX/this.props.width);
                this.ani = TweenMax.set(this.refs.menu, {style:{right:posX}});
                TweenMax.set(this.refs.blockade, {style:{opacity:percent}});
            }
        }
        onRelease(e, state){
            if(this.props.position == NavBar.LEFT){
                let validSwipe = this.isValidSwipe(state.vx, this.swipeConfig.velocityThreshold, state.dy, this.swipeConfig.directionalOffsetThreshold);
                if(validSwipe){
                    if(state.dx < 0) this.close();
                    else this.open();
                }else{
                    let percent = Math.abs(state.dx/this.props.width);
                    if(percent > 0.5) this.close();
                    else this.open();

                }
            }else{
                let validSwipe = this.isValidSwipe(state.vx, this.swipeConfig.velocityThreshold, state.dy, this.swipeConfig.directionalOffsetThreshold);
                if(validSwipe){
                    if(state.dx > 0) this.close();
                    else this.open();
                }else{
                    let percent = Math.abs(state.dx/this.props.width);
                    if(percent > 0.5) this.close();
                    else this.open();
                }
            }
        }

        isClick(state) {
            let result = Math.abs(state.dx) < this.swipeConfig.gestureIsClickThreshold && Math.abs(state.dy) < this.swipeConfig.gestureIsClickThreshold;
            return result;
        }

        isValidSwipe(velocity, velocityThreshold, directionalOffset, directionalOffsetThreshold) {
            return Math.abs(velocity) > velocityThreshold && Math.abs(directionalOffset) < directionalOffsetThreshold;
        }

        render(){
            return(
                <View ref="root" style={[CStyle.cover, {elevation: 10, overflow:"hidden"}, this.props.styleNavBar]} {...this.panResponder.panHandlers}  >
                    <TouchableWithoutFeedback onPress={()=>{ this.close(); }}>
                        <View ref="blockade" style={[CStyle.blockade]}></View>
                    </TouchableWithoutFeedback>
                    <View ref="menu" style={[CStyle.cover, {backgroundColor:"#FFF", maxWidth:this.props.width, width:"100%", right:(this.props.position == NavBar.LEFT) ? "auto" : 0, left:(this.props.position == NavBar.RIGHT) ? "auto" : 0, elevation:15}, this.props.style]} >
                        {this.props.content}
                    </View>
                </View>
            )
        }
    }

/* Switch */
    export class Switch extends Component{
        static propTypes ={checked:PropTypes.bool, disabled:PropTypes.bool, callback:PropTypes.func, style:PropTypes.any, styleBall:PropTypes.any, background:PropTypes.string, backgroundChecked:PropTypes.string};
        static defaultProps =   { checked:false, disabled:false, callback: null, style:{}, styleBall:{}, background:"#BBB", backgroundChecked:"#E85975"};

        constructor(props) {
            super(props);
            let style = cuppa.mergeObjects([{width:50, height:31, backgroundColor:"#E85975", borderRadius:32, justifyContent:"center", alignItems:"flex-start"}, this.props.style]);
            let styleBall = cuppa.mergeObjects([{borderRadius:22, width:22, height:22, backgroundColor:"#EBEBEB", left:4, elevation:4}, this.props.styleBall]);
            this.state = {checked:false, style:style, styleBall:styleBall};
        }

        componentDidMount(){
            this.checked(this.props.checked, 0, true);
            if(this.props.disabled) TweenMax.set(this.refs.cover, {style:{opacity:0.6}});
        }

        componentWillUpdate(nextProps, nextState) {
            if(nextProps.checked != this.props.checked){
                this.checked(nextProps.checked, 0.3, true);
            }
        }


        checked = function(value, duration = 0.3, silence = false){
            if(value == undefined) value = !this.state.checked;
            this.setState({checked:value});

            if(value){
                let pos = this.state.style.width-this.state.styleBall.width-this.state.styleBall.left;
                TweenMax.to(this.refs.cover, duration, {style: {backgroundColor:this.props.backgroundChecked} } );
                TweenMax.to(this.refs.ball, duration, {style: {left:pos} } );
            } else{
                let pos = this.state.styleBall.left;
                TweenMax.to(this.refs.cover, duration, {style: {backgroundColor:this.props.background} } );
                TweenMax.to(this.refs.ball, duration, {style: {left:pos} } );
            }

            if(silence == false && this.props.callback) this.props.callback(value);
        }.bind(this);

        render() {
            return (
                <TouchableWithoutFeedback disabled={this.props.disabled} onPress={()=>{ this.checked(); }} >
                    <View ref={"cover"} style={[this.state.style]}>
                        <View ref={"ball"} style={[this.state.styleBall]} ></View>
                    </View>
                </TouchableWithoutFeedback>
            )
        }
    }

/* DropDown
    <DropDown visible={false} ref={"dropdown"} style={{right:5, top:5}}>
        <Button onPress={ (e)=>this.refs.dropdown.close() } text="Add Item" />
        <Button onPress={ (e)=>this.refs.dropdown.close() } text="Remove Item" />
    </DropDown>
*/
    export class DropDown extends Component{
        static propTypes = {style:PropTypes.any, visible:PropTypes.b, onOpen:PropTypes.func, onClose:PropTypes.func};
        static defaultProps = {style:{}, visible:true, onOpen:null, onClose:null};

        constructor(props) {
            super(props);
            this.state = {modalVisible: true,}
            this.childrens = props.children;
            if (!Array.isArray(this.childrens)) this.childrens = [props.children];
        }

        componentDidMount(){
            if(this.props.visible === false) this.close(0);
        }

        open = function(duration = 0.4){
            if(this.tl) this.tl.kill();
            this.tl = new TimelineMax();
            this.tl.set(this.refs.root, {style:{top:0}});
            this.tl.fromTo(this.refs.dropdown, duration, { style:{opacity:0} }, { style:{opacity:1}, ease:Cubic.easeOut });
            if(this.props.onOpen) this.props.onOpen();
        }.bind(this)

        close = function(duration = 0.3){
            if(this.tl) this.tl.kill();
            this.tl = new TimelineMax();
            this.tl.fromTo(this.refs.dropdown, duration, { style:{opacity:1} }, { style:{opacity:0}, ease:Power2.easeIn });
            this.tl.set(this.refs.root, {style:{top:9999}});
            if(this.props.onClose) this.props.onClose();
        }.bind(this)

        render(){
            return (
                <View ref={"root"} style={[CStyle.blockade,{ backgroundColor:"rgba(0,0,0,0)" }]}>
                    <TouchableWithoutFeedback onPress={()=>{ this.close(); }} >
                        <View style={[CStyle.blockade, {backgroundColor:"rgba(0,0,0,0)"}]}></View>
                    </TouchableWithoutFeedback>
                    <View ref={"dropdown"} style={[{position:"absolute", backgroundColor:"#FFF", overflow: "hidden", opacity:1, elevation:4}, this.props.style]}>
                        {
                            this.childrens.map((element, key) => {
                                return (element)
                            })
                        }
                    </View>
                </View>
            )
        }
    }

/* PickList
    <PickList key="pickList">
        <Button onPress={this.onPress.bind(this)} text="Camera" textStyle={{textAlign:"left"}} />
        <Button text="Gallery" textStyle={{textAlign:"left"}} />
    </PickList>
 */
    export class PickList extends Component {
        static propTypes = {height:PropTypes.number, maxHeight:PropTypes.number, style:PropTypes.any};
        static defaultProps = {height:300, maxHeight:300, style:{}};

        constructor(props) {
            super(props);
            this.state = {}
            this.childrens = props.children;
            if (!Array.isArray(this.childrens)) this.childrens = [props.children];
        }

        componentDidMount(){
            this.open();
        }

        open(){
            if(this.tl) this.tl.kill();
            this.tl = new TimelineMax();
            this.tl.fromTo(this.refs.blockade, 0.4, {style:{opacity:0}}, {style:{opacity: 1}, ease: Cubic.easeOut});
            this.tl.fromTo(this.refs.menu, 0.4, { style:{bottom:-this.getHeight(), opacity:0} }, { style:{bottom:0, opacity:1}, ease:Expo.easeOut }, 0);
        }

        close(){
            if(this.tl) this.tl.kill();
            this.tl = new TimelineMax();
            this.tl.fromTo(this.refs.blockade, 0.3, {opacity:1}, { style:{opacity:0}, ease:Linear.easeNone } );
            this.tl.fromTo(this.refs.menu, 0.3, {style:{opacity:1, bottom:0}}, { style:{bottom:-this.getHeight(), opacity:0}, ease:Expo.easeIn }, 0);
            this.tl.add(()=>{
                this.tl = null;
                if(this.props.onClose) this.props.onClose();
            });
        }

        getHeight(){
            let value = this.props.maxHeight;
            if(this.props.height < value) value = this.props.height;
            return value;
        }

        render() {
            return (
                <View ref="pickList" style={[{position: "absolute", top: 0, left: 0, right: 0, elevation: 2, bottom: 0, height:"100%", overflow: "hidden"}, this.props.style]}>
                    <TouchableWithoutFeedback onPress={this.close.bind(this)} >
                        <View ref="blockade" style={{position: "absolute", top: 0, left: 0, right: 0, bottom: 0, opacity:0, backgroundColor: "rgba(0,0,0,0.5)"}}></View>
                    </TouchableWithoutFeedback>
                    <View ref="menu" style={[CStyle.pickListMenu, {bottom: -this.getHeight()}]}>
                        <ScrollView contentContainerStyle={{padding: 0}}>
                            {
                                this.childrens.map((element, key) => {
                                    return (
                                        <View key={key}>
                                            {element}
                                        </View>
                                    )
                                })
                            }
                        </ScrollView>
                    </View>
                </View>
            )
        }
    }

/* StatusBarComp
    Example: <StatusBarGap background={"rgba(0,0,0,0.2)"} color={"light-content"} />
*/
    export class StatusBarComp extends Component {
        static propTypes = {background:PropTypes.string, color:PropTypes.oneOf(['light-content', 'dark-content'])};
        static defaultProps = { background:"rgba(0,0,0,0.2)", color:'dark-content' };
        constructor(props) {
            super(props);
            this.config();
        }

        componentDidMount(){
            this.config();
        }

        config(){
            this.height = StatusBarComp.height();
            StatusBar.setBarStyle(this.props.color);
            if (Platform.OS == "android" && Platform.Version >= 21){
                StatusBar.setBackgroundColor('rgba(0,0,0,0)');
                StatusBar.setTranslucent(true);
            }
        }

        static height(){
            let value = 0;
            if(Platform.OS == "ios") {
                value = getStatusBarHeight();
            }else{
                if(Platform.Version >= 21){
                    value = StatusBar.currentHeight;
                }
            }
            return value;
        }

        static barStyle(style = "light-content"){
            StatusBar.setBarStyle(style);
        }

        render() {
            return (
                <View style={{height:this.height, width:"100%", backgroundColor:this.props.background}}></View>
            );
        }
    }

/* ModalAlert
    <Button onPress={ ()=>{ this.refs.roomModal.open(); }} text="Open ModalAlert" />
    <ModalAlert key="modalAlert" ref={(modalAlert)=>{ this.modalAlert = modalAlert }}  >
        <Button onPress={ ()=>{ this.modalAlert.close(); }} text="Option1" />
        <Button onPress={ ()=>{ this.modalAlert.close(); }} text="Option2" />
    </ModalAlert>
 */
    export class ModalAlert extends Component{
        static propTypes = {onClose:PropTypes.func, visible:PropTypes.bool, styleBlockade:PropTypes.any, style:PropTypes.any};
        static defaultProps = { visible:false, styleBlockade:{}, style:{} };
        constructor(props) {
            super(props);
            this.state = {visible:props.visible};
            this.childrens = props.children;
            if(!Array.isArray(this.childrens)) this.childrens = [props.children];
        }

        componentDidMount(){
            this.open();
        }

        open = function(){
            TweenMax.set(this.refs.menu, { transform:{scale:0.8}, style:{opacity:0 } } );
            TweenMax.set(this.refs.blockade, {style:{opacity:0} } );
            this.tl = new TimelineMax();
            this.tl.to(this.refs.blockade, 0.3, {style:{opacity: 1}, ease: Power2.easeOut});
            this.tl.to(this.refs.menu, 0.3, { transform:{scale:1}, style:{opacity:1}, ease:Expo.easeOut }, 0.1 );
        }.bind(this);

        close = function(){
            if(this.tl) this.tl.kill();
            this.tl = new TimelineMax();
            this.tl.to(this.refs.menu, 0.3, { style:{opacity:0}, transform:{scale:0.8}, ease:Expo.easeIn } );
            this.tl.to(this.refs.blockade, 0.3, { style:{opacity:0}, ease:Power0.easeInOut } );
            this.tl.add(()=>{
                this.setState({visible:false});
                this.tl = null; if(this.props.onClose) this.props.onClose();
            });
        }.bind(this);

        render(){
            return(
                <View style={{position:"absolute", top:0, left:0, right:0, bottom:0, flex:1, elevation:20, justifyContent:"center", alignItems: "center", padding:20}} >
                    <BackComponet callback={ this.close } />
                    <TouchableWithoutFeedback onPress={this.close} >
                        <View ref="blockade" style={[CStyle.blockade, this.props.styleBlockade]} />
                    </TouchableWithoutFeedback>
                    <View ref="menu" style={[{backgroundColor:"#FFF", width:"100%", opacity:0, borderRadius:3, maxHeight:Dimensions.get('window').height, margin:20, overflow:"hidden"}, this.props.style]}>
                        <ScrollView style={{width:"100%"}} contentContainerStyle={{padding:0}}>
                            {
                                this.childrens.map((element, key)=>{
                                    return(<View key={key} >{element}</View>)
                                })
                            }
                        </ScrollView>
                    </View>
                </View>
            )
        }
    }
/* ModalManager
    <ModalManager ref={ ref => Globals.mainModal = ref } />

    Globals.mainModal.add(<Text>Hello</Text>, 'myElement');
    Globals.mainModal.remove('myElement');

    Globals.mainModal.add(<Text ref={ref=>{ Globals.mainModal.addRef('myElement', ref) } } >Hello</Text>, 'myElement');
    Globals.mainModal.get('myElement');
    Globals.mainModal.getRef('myElement');

*/
    export class ModalManager extends Component{
        static defaultProps = { }

        constructor(props) {
            super(props);
            this.state = {elements:{}, references:{} };
        }

        add(name, element){
            if(!name) name = "modal";
            let elements = this.state.elements;
            elements[name] = element;
            this.setState({elements:elements});
        }

        remove(name){
            if(!name) name = "modal";
            let elements = this.state.elements;
            let ref = this.state.references[name];
            delete elements[name];
            this.setState({elements:elements});
            delete this.state.references[name];
        }

        get(name){
            if(!name) name = "modal";
            return this.state.elements[name];
        }

        addRef(name, ref){
            if(!name) name = "modal";
            this.state.references[name] = ref;
        }

        getRef(name){
            if(!name) name = "modal";
            return this.state.references[name];
        }

        removeAll(){
            this.setState({references:{}});
            this.setState({elements:{}});
        }

        render(){
            let elements = [];
            for (let name in this.state.elements) {
                elements.push(this.state.elements[name]);
            }
            return elements;
        }
    }

/* BackComponet
    <BackComponet />
*/
    export class BackComponet extends Component{
        static propTypes = {callback:PropTypes.func, closeApp:PropTypes.bool};
        static defaultProps = {closeApp:false, callback:null};

        constructor(props) {
            super(props);
            this.state = {  };
        }

        componentDidMount(){
            BackHandler.addEventListener('hardwareBackPress', this.onHandler);
        }

        onHandler = function(){
            if(this.props.callback) this.props.callback();

            if(!this.props.closeApp) return true;
            else return false;
        }.bind(this);

        componentWillUnmount(){
            BackHandler.removeEventListener('hardwareBackPress', this.onHandler);
        }
        render(){ return null }
    }

/* mergeObjects, create a new obj with the values of objs in Array.
    If create_new_object = true, create a new Oject an Add all element to it, else join to the first object all elements
    create_new_object = false
*/
    cuppa.mergeObjects = function(array_objs, create_new_object){
        if(!create_new_object){
            var obj1 = array_objs.shift();
            for(var i = 0; i < array_objs.length; i++){
                var obj = array_objs[i];
                if(obj){ for (var attrname in obj) { obj1[attrname] =  obj[attrname]; } }
            };
            return obj1;
        }else{
            var tmp_obj = {};
            for(var i = 0; i < array_objs.length; i++){
                var obj = array_objs[i];
                if(obj){ for (var attrname in obj) { tmp_obj[attrname] = obj[attrname]; } }
            };
            return tmp_obj;
        };
    };

// mergeArray, create a new array with the values all array passed.
    cuppa.mergeArrays = function(arrays){
        let result = [];
        for(var i = 0; i < arrays.length; i++){
            if(arrays[i]){
                result = result.concat(arrays[i]);
            };
        };
        return result;
    };

/*  dataCenter
    add and remove data in one place
    Examples:
    cuppa.setData('NEW_CLIENT', {data:{name:"Tufik", age":18}})
    cuppa.getData('NEW_CLIENT', {callback:listener});
*/
    cuppa.dataDefault = {};
    cuppa.data = {};

    cuppa.setData = async function(name, opts){
        opts = cuppa.mergeObjects([{storage:'', store:undefined, silence:false, data:null, "default":null}, opts]);
        if(opts.store != undefined) opts.storage = opts.store;

        if(opts["default"] !== null){
            cuppa.dataDefault[name] = opts["default"];
            opts["default"] = null;
            let current = await cuppa.getData(name, opts);
            if(current == null || current == undefined) current = cuppa.dataDefault[name];
            opts.data = current;
            cuppa.setData(name, opts);
            return;
        }
        cuppa.data[name] = opts.data;
        if(opts.storage === "local"){
            try { await AsyncStorage.setItem(name, JSON.stringify(opts.data)); } catch (error) { }
        };

        if(!opts.silence) cuppa.executeListener(name, opts.data);
    };

    cuppa.getData = async function(name, opts){
        opts = cuppa.mergeObjects([{storage:'', store:undefined, callback:null, "default":false}, opts]);
        if(opts.store != undefined) opts.storage = opts.store;

        if(opts["default"]){ return cuppa.dataDefault[name]; }

        var data = cuppa.data[name];
        if(opts.storage === "local"){
            data = JSON.parse(await AsyncStorage.getItem(name));
        }

        if(data != undefined && opts.callback){
            opts.callback(data);
        }
        if(opts.callback){
            cuppa.addListener(name, opts.callback);
        };
        return data;
    };


    cuppa.getDataSync = function(name, opts){
        opts = cuppa.mergeObjects([{default:false}, opts]);
        var data = cuppa.data[name];
        if(opts["default"]) data = cuppa.dataDefault[name];
        return data;
    };


    cuppa.deleteData = function(name, opts){
        opts = cuppa.mergeObjects([{storage:'', store:undefined}, opts]);
        if(opts.store != undefined) opts.storage = opts.store;

        cuppa.data[name] = null;
        if(opts.storage === "local"){
            AsyncStorage.removeItem(name);
        };
    };

// add / remove / execute global listeners
    cuppa.listeners = {};
    cuppa.addListener = function(name, callback){
        if(!cuppa.listeners[name]) cuppa.listeners[name] = [];
        cuppa.listeners[name].push(callback);
    };

    cuppa.removeListener = function(name, callback, toString){
        if(!cuppa.listeners[name]) cuppa.listeners[name] = [];
        var array =  cuppa.listeners[name];
        for(var i = 0 ; i < array.length; i++ ){
            if(toString){
                if(array[i].toString() === callback.toString()){
                    array.splice(i, 1);
                };
            }else{
                if(array[i] === callback){
                    array.splice(i, 1);
                };
            }
        };
    };

    cuppa.removeListenerGroup = function(name){
        delete cuppa.listeners[name];
    };

    cuppa.executeListener = function(name, data){
        if(!cuppa.listeners[name]) cuppa.listeners[name] = [];
        var array =  cuppa.listeners[name];
        for(var i = 0 ; i < array.length; i++ ){
            array[i](data);
        };
    };

/* Clone object */
    cuppa.cloneObject = function(object, useJSON){
        if(useJSON == undefined) useJSON = true;
        var object2;
        if(useJSON) object2 = JSON.parse(JSON.stringify(object));
        else object2 = Object.assign({}, object);
        return object2;
    };
// ObjectToURL
    cuppa.objectToURL = function(object){
        var str = "";
        for(var key in object) {
            str += "&";
            if(object[key]) str += key + "=" + (object[key]);
            else str += key;
        };
        return str;
    };
// object Length
    cuppa.objectLength = function(object){
        return Object.keys(object).length;
    };
// clone Array
    cuppa.cloneArray = function(array){
        return array.slice(0);
    };
// clone Array
    cuppa.capitalize = function(value){
        value = String(value) || "";
        return value && value[0].toUpperCase() + value.slice(1);
    }

// dimenstion
    cuppa.dim = function(){
        return Dimensions.get('window');
    }

/* ajax
    opts{
        data:Object
        method:"POST" // GET
        async: true // true,false
        header:{key:value, key:value}
        onError:callback
    }
*/
    cuppa.ajax = function(url, opts, callback){
        // opts
        opts = opts || { };
        opts.method = (opts.method) ? opts.method.toUpperCase() : "POST";
        if(opts.async == undefined) opts.async = true;
        var request = new XMLHttpRequest();
        request.open(opts.method, url, opts.async);
        //++ add header
        request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        if(opts.header){
            for (var key in opts.header) {
                request.setRequestHeader(key, opts.header[key]);
            };
        };
        //--
        request.addEventListener("load", onComplete);
        request.addEventListener("error", onError);
        if(opts.data && opts.row) request.send(opts.data);
        else if(opts.data) request.send(cuppa.objectToURL(opts.data));
        else request.send();
        // onComplete
        function onComplete () {
            var result = this.responseText;
            if(opts.callback) opts.callback(result, this);
            if(callback) callback(result, this);
        };
        // onError
        function onError(e){
            if(opts.onError) opts.onError(e);
        };
        return request;
    };

/* get Filename
ex: dir/dir/name.txt, return {name:name, ext:txt, type:txt}
*/
    cuppa.getFileDescription = function(file, forced){
        if (!file) return null;
        if(Array.isArray(file)) file = file[0];
        var separator = "/";
        file =  cuppa.replace(file, "\\\\", "/");
        var array = file.split(separator);
        if (cuppa.trim(array[array.length - 1]) === "") array.pop();
        var obj = {};
        obj.name_complete = array.pop();
        obj.path = array.join(separator) + separator;
        obj.file = obj.path + obj.name_complete;
        var array2 = obj.name_complete.split(".");
        if (array2.length > 1 || forced === "file" ) {
            obj.type = "file";
            if (array2.length > 1) obj.extension = array2.pop().toLowerCase();
            else obj.extension = "";
            obj.name = array2.join(".");
        }else{
            obj.type = "folder";
        }
        return obj;
    };
    cuppa.fileDescription = function(filePath){ return cuppa.getFileDescription(filePath); }

/* replace in range
opts = {
first: false,
range: [init_string, end_string],
first_range: false
split: false;
}
*/
    cuppa.replace = function(string, search, replace, opts){
        if(!string) return "";
        opts = opts || {};
        if(opts.range){
            var c_temp1 = string.split(opts.range[0]);
            if(c_temp1.length <= 1 ) return string;
            for(var i = 0; i < c_temp1.length; i++){
                var c_temp2 = c_temp1[i].split(opts.range[1]);
                if(c_temp2.length > 1){
                    if(opts.first){
                        if(opts.split) c_temp2[0] = cuppa.replaceSplit(c_temp2[0], search, replace);
                        else c_temp2[0] = c_temp2[0].replace(search, replace);
                    }else{
                        if(opts.split) c_temp2[0] = cuppa.replaceSplit(c_temp2[0], search, replace);
                        else c_temp2[0] = c_temp2[0].replace(new RegExp(search, 'g'), replace);
                    }
                    c_temp1[i] = c_temp2.join(opts.range[1]);
                    if(opts.first_range) break;
                }
            }
            string = c_temp1.join(opts.range[0]);
        }else{
            if(opts.first){
                if(opts.split) string = cuppa.replaceSplit(string, search, replace);
                else string = string.replace(search, replace);
            }else{
                if(opts.split) string = cuppa.replaceSplit(string, search, replace);
                else string = string.replace(new RegExp(search, 'g'), replace);
            }
        };
        return string;
    };
    cuppa.replaceSplit = function(string, search, replace){
        return string.split(search).join(replace);
    };

/* Default animations
    show> this.ani = cuppa.slideShow({ref:this.refs.main, direction:"up"});
    hide> this.ani = cuppa.slideHide({ref:this.ani.ref, tween:this.ani.tween, direction:"down", callback:()=>{ } });
 */
    cuppa.slideShow = function(opts){
        opts = cuppa.mergeObjects([{ref:null, blockade:null, direction:"up", duration:0.4, delay:0.2, ease:Expo.easeOut, callback:null}, opts]);
        if(!opts.ref) return;
        // set
        if(opts.direction == "up") TweenMax.set(opts.ref, {transform:{y:cuppa.dim().height, x:0}} );
        if(opts.direction == "down") TweenMax.set(opts.ref, {transform:{y:-cuppa.dim().height, x:0}} );
        if(opts.direction == "left") TweenMax.set(opts.ref, {transform:{x:cuppa.dim().width, y:0}} );
        if(opts.direction == "right") TweenMax.set(opts.ref, {transform:{x:-cuppa.dim().width, y:0}} );
        if(opts.blockade) TweenMax.set(opts.blockade, {style:{opacity:0}});
        // animate
        opts.tween = new TimelineMax({onComplete:opts.callback});
        opts.tween.to(opts.ref, opts.duration, { transform: {y:0, x:0}, ease:opts.ease, delay:opts.delay}, 0);
        if(opts.blockade) opts.tween.to(opts.blockade, opts.duration, {style:{opacity: 1}, ease:Power0.easeInOut, delay:opts.delay}, 0);
        opts.ref.tweenData = opts;
        return opts;
    }
    cuppa.slideHide = function(opts){
        opts = cuppa.mergeObjects([{ref:null, tween:null, blockade:null, direction:"down", duration:0.3, delay:0, ease:Expo.easeIn}, opts]);
        if(!opts.ref) return;
        if(opts.ref.tweenData) opts.ref.tweenData.tween.kill();
        opts.tween = new TimelineMax({onComplete:opts.callback});
        // set
        if(opts.blockade && !opts.ref.tweenData) opts.tween.set(opts.blockade, {style:{opacity:1}});
        if(!opts.ref.tweenData) opts.tween.set(opts.ref, {transform:{x:0, y:0} } );
        // animate
        if(opts.direction == "up") opts.tween.to(opts.ref, opts.duration, { transform: {y:-cuppa.dim().height, x:0}, delay:opts.delay, ease:opts.ease}, 0);
        else if(opts.direction == "down") opts.tween.to(opts.ref, opts.duration, { transform: {y:cuppa.dim().height, x:0}, delay:opts.delay, ease:opts.ease}, 0);
        else if(opts.direction == "left") opts.tween.to(opts.ref, opts.duration, { transform: {x:-cuppa.dim().height, y:0}, delay:opts.delay, ease:opts.ease}, 0);
        else if(opts.direction == "right") opts.tween.to(opts.ref, opts.duration, { transform: {x:cuppa.dim().width, y:0}, delay:opts.delay, ease:opts.ease}, 0);
        if(opts.blockade) opts.tween.to(opts.blockade, opts.duration, { style:{opacity:0}, ease:Power0.easeInOut }, 0.2);
        opts.ref.tweenData = opts;
        return opts;
    }
    cuppa.popShow = function(opts){
        opts = cuppa.mergeObjects([{ref:null, tween:null, blockade:null, duration:0.25, delay:0.2, ease:Power2.easeOut}, opts]);
        if(!opts.ref) return;
        // set
        TweenMax.set(opts.ref, { transform:{scale:0.8}, style:{opacity:0 } });
        if(opts.blockade) TweenMax.set(opts.blockade, {style:{opacity:0}});
        // animate
        opts.tween = new TimelineMax({onComplete:opts.callback});
        opts.tween.to(opts.ref, opts.duration, { transform:{scale:1}, style:{opacity:1}, ease:opts.ease, delay:opts.delay}, 0);
        if(opts.blockade) opts.tween.to(opts.blockade, opts.duration, {style:{opacity: 1}, ease:opts.ease, delay:opts.delay}, 0);
        opts.ref.tweenData = opts;
        return opts;
    }
    cuppa.popHide = function(opts){
        opts = cuppa.mergeObjects([{ref:null, tween:null, blockade:null, duration:0.25, delay:0, ease:Expo.easeIn}, opts]);
        if(!opts.ref) return;
        if(opts.ref.tweenData) opts.ref.tweenData.tween.kill();
        opts.tween = new TimelineMax({onComplete:opts.callback});
        // set
        if(opts.blockade && !opts.ref.tweenData) opts.tween.set(opts.blockade, {style:{opacity:1}});
        if(!opts.ref.tweenData) opts.tween.set(opts.ref,  {transform:{scale:1}, style:{opacity:1}});
        // animate
        opts.tween.to(opts.ref, opts.duration, { style:{opacity:0}, transform:{scale:0.8}, delay:opts.delay, ease:opts.ease}, 0 );
        if(opts.blockade) opts.tween.to(opts.blockade, opts.duration, { style:{opacity:0}, delay:opts.delay, ease:Power0.easeInOut } );
        opts.ref.tweenData = opts;
        return opts;
    }
// JSON Decode
    cuppa.jsonDecode = function(value, base64_decode){
        if(value === undefined || value === null) return "";
        if(base64_decode === undefined) base64_decode = true;
        if(base64_decode) value = Base64.decode(value);
        try{ value = JSON.parse(value); }catch(err){}
        return value;
    };
// JSON Encode
    cuppa.jsonEncode = function(value, base64_encode){
        if(value === undefined || value === null) return "";
        if(base64_encode === undefined) base64_encode = true;
        try{ value = JSON.stringify(value); }catch(err){}
        if(base64_encode) value = Base64.encode(value);
        return value;
    };
// Base64 Decode
    cuppa.base64Decode = function(value){ if(!value) return ''; return Base64.decode(value); };
//  Base64 Encode
    cuppa.base64Encode = function(value){ if(!value) return ''; return Base64.encode(value); };

/* remove html tags */
    cuppa.removeHTMLTags = function(cleanText){
        cleanText = cleanText.replace(/<\/?[^>]+(>|$)/g, "");
        return cleanText;
    };
/* Cut text */
    cuppa.cutText = function(delimiter, text, lenght, string_to_end, delimiter_forced, remove_tags){
        if(text === undefined) text = "";
        if(delimiter === undefined) delimiter = " ";
        if(lenght === undefined) lenght = 200;
        if(string_to_end === undefined) string_to_end = "";
        if(delimiter_forced === undefined) delimiter_forced = false;
        if(remove_tags === undefined) remove_tags = false;
        if(remove_tags) text = cuppa.removeTags(text);

        if(delimiter_forced){
            text = text.substr(0, lenght);
            if(cuppa.strrpos(text, delimiter) !== false) text = cuppa.trim(text.substr(0, cuppa.strrpos(text, delimiter)));
            text += string_to_end;
        }else if(text.length > lenght ){
            text = text.substr(0, lenght);
            if(cuppa.strrpos(text, delimiter) !== false) text = cuppa.trim(text.substr(0, cuppa.strrpos(text, delimiter)));
            text += string_to_end;
        }
        return text;
    };
/* Strrpost */
    cuppa.strrpos = function(haystack, needle, offset) {
        var i = -1;
        if (offset) {
            i = (haystack + '')
                .slice(offset)
                .lastIndexOf(needle);
            if (i !== -1) {
                i += offset;
            }
        } else {
            i = (haystack + '')
                .lastIndexOf(needle);
        }
        return i >= 0 ? i : false;
    };

/* Method Trim() */
    cuppa.trim = function(string){
        if(string) return string.replace(/^\s+|\s+$/g, '');
        else return "";
    };

/* Validate if a string is email, Return true of false */
    cuppa.email = function(value){
        var emailExpression = /^[a-z 0-9][\w.-]+@\w[\w.-]+\.[\w.-]*[a-z][a-z]$/i;
        return emailExpression.test(cuppa.trim(value));
    };

/* Get percent betwen 2 range */
    cuppa.percent = function(n, min, max, invert){
        var percent = (n-min)/(max-min);
        if(percent < 0) percent = 0;
        else if(percent > 1) percent = 1;
        if(invert) percent = 1-percent;
        return percent
    };

/* Capitalize */
    cuppa.capitaliseFirstLetter = function(string){
        string = string.toLocaleLowerCase();
        return string.charAt(0).toUpperCase() + string.slice(1);
    };
    cuppa.capitaliseAllWords = function(str){
        str = str.toLocaleLowerCase();
        var pieces = str.split(" ");
        for ( var i = 0; i < pieces.length; i++ ){
            var j = pieces[i].charAt(0).toUpperCase();
            pieces[i] = j + pieces[i].substr(1);
        }
        return pieces.join(" ");
    };

/* Get UrlFriendly
    Example: news/article-news-1
*/
    cuppa.urlFriendly = function(str, max, spaceChar) {
        if(spaceChar == undefined) spaceChar = "-";
        if(!str) return;
        if (max === undefined) max = 500;
        var a_chars = [];
        a_chars.push(["a",/[áàâãªÁÀÂÃ]/g]);
        a_chars.push(["e",/[éèêÉÈÊ]/g]);
        a_chars.push(["i",/[íìîÍÌÎ]/g]);
        a_chars.push(["o",/[òóôõÓÒÔÕ]/g]);
        a_chars.push(["u",/[úùûÚÙÛ]/g]);
        a_chars.push(["c",/[çÇ]/g]);
        a_chars.push(["n",/[Ññ]/g]);
        a_chars.push(["-",/[.]/g]);
        a_chars.push(["",/['"\\()[\]/*++¿?#:;@$º&*^·’,.!¡%=+|]/g]);
        for(var i=0; i < a_chars.length; i++){
            str = str.replace(a_chars[i][1], a_chars[i][0]);
        }
        str = str.replace(/\s+/g,spaceChar).replace(/_+/g,spaceChar).toLowerCase().replace(/-{2,}/g,spaceChar).replace(/(^\s*)|(\s*$)/g, '').substr(0,max);
        str = str.replace(/[^a-zA-Z0-9_ -]/g, "");
        str = str.replace(/[\-]/g, spaceChar);
        return str;
    };
    cuppa.cleanString = function(str, max, spaces){ return cuppa.urlFriendly(str, max, spaces); };
    cuppa.removeSpecialCharacters = function(str, max, spaces){ return cuppa.urlFriendly(str, max, spaces); };

export var CStyle = StyleSheet.create({
    pickListMenu: { position: "absolute", bottom: 0, left: 0, right: 0, backgroundColor: "#FFF", width: "100%", maxHeight: 300, elevation: 2, shadowColor: '#000', shadowOffset: {width: 0, height: 0}, shadowOpacity: 0.2,},
    pickListMenuButton:{ height:50, paddingLeft:20, },
    cover:{ position:"absolute", top:0, left:0, right:0, bottom:0, },
    wire:{borderWidth: 2, borderColor:"#000", borderStyle:"dotted", borderRadius: 0.001,},
    wire_white:{borderWidth: 2, borderColor:"#fff", borderStyle:"dotted", borderRadius: 0.001,},
    blockade:{position:"absolute", top:0, left:0, right:0, bottom:0, backgroundColor:"rgba(0,0,0,0.7)"},
    t10:{fontSize:10}, t12:{fontSize:12}, t14:{fontSize:14}, t16:{fontSize:16}, t18:{fontSize:18}, t20:{fontSize:20},
    fBold:{ fontWeight: "bold"},
    paddingX5:{paddingLeft:5, paddingRight: 5}, paddingY5:{paddingTop:5, paddingBottom: 5},
    paddingX10:{paddingLeft:10, paddingRight: 10}, paddingY10:{paddingTop:10, paddingBottom: 10},
    paddingX15:{paddingLeft:15, paddingRight: 15}, paddingY15:{paddingTop:15, paddingBottom: 15},
    paddingX20:{paddingLeft:20, paddingRight: 20}, paddingY20:{paddingTop:20, paddingBottom: 20},
    paddingX25:{paddingLeft:25, paddingRight: 25}, paddingY25:{paddingTop:25, paddingBottom: 25},
    paddingX30:{paddingLeft:30, paddingRight: 30}, paddingY30:{paddingTop:30, paddingBottom: 30},

    marginX5:{marginLeft:5, marginRight: 5}, marginY5:{marginTop:5, marginBottom: 5},
    marginX10:{marginLeft:10, marginRight: 10}, marginY10:{marginTop:10, marginBottom: 10},
    marginX15:{marginLeft:15, marginRight: 15}, marginY15:{marginTop:15, marginBottom: 15},
    marginX20:{marginLeft:20, marginRight: 20}, marginY20:{marginTop:20, marginBottom: 20},
    marginX25:{marginLeft:25, marginRight: 25}, marginY25:{marginTop:25, marginBottom: 25},
    marginX30:{marginLeft:30, marginRight: 30}, marginY30:{marginTop:30, marginBottom: 30},

    positionLT:{ position:"absolute", top:0, left:0 }, absoluteLT:{ position:"absolute", top:0, left:0 },
    positionRT:{ position:"absolute", top:0, right:0 }, absoluteRT:{ position:"absolute", top:0, right:0 },
    positionLB:{ position:"absolute", bottom:0, left:0 }, absoluteLB:{ position:"absolute", bottom:0, left:0 },
    positionRB:{ position:"absolute", bottom:0, right:0 }, absoluteRB:{ position:"absolute", bottom:0, right:0 },
    positionRC:{ position:"absolute", top:"50%", right:0 }, absoluteRC:{ position:"absolute", top:"50%", right:0 },
    positionLC:{ position:"absolute", top:"50%", right:0 }, absoluteLC:{ position:"absolute", top:"50%", left:0 },
    positionCT:{ position:"absolute", top:0, left:"50%" }, absoluteCT:{ position:"absolute", top:0, left:"50%" },
    positionCB:{ position:"absolute", bottom:0, left:"50%" }, absoluteCB:{ position:"absolute", bottom:0, left:"50%" },
    positionCC:{ position:"absolute", top:"50%", left:"50%" }, absoluteCC:{ position:"absolute", top:"50%", left:"50%" },
    bgWarning:{backgroundColor:"#F0AD4E"}, colorWarning:{ color:"#F0AD4E" }, tintColorWarning:{tintColor: "#F0AD4E" },
    bgError:{backgroundColor:"#ed5666"}, colorError:{ color:"#ed5666" }, tintColorError:{tintColor: "#ed5666" },
    bgSuccess:{backgroundColor:"#5CB85C"}, colorSuccess:{ color:"#5CB85C" }, tintColorSuccess:{tintColor: "#5CB85C" },
    headerBar:{ backgroundColor:"#4050B5", height:cuppa.sDim(0, 43, "h").height, flexDirection: "row", justifyContent: "flex-start", alignItems: "center" },
    headerText:{ fontSize:18, color:"#FFF", },
    headerButton:{ width:cuppa.sDim(0, 43, "h").height, height:cuppa.sDim(0, 43, "h").height },
    headerIcon:{height:cuppa.sDim(16,16,"h").height, width:"100%", tintColor:"#FFF"},
    tabBar:{ backgroundColor:"#FFF", height:cuppa.sDim(0, 43, "h").height, flexDirection: "row", justifyContent: "space-around", alignItems: "center", elevation:2, },
    tabBarButton:{ width:"100%", height:"100%", flex:1 },
    tabBarIcon:{height:cuppa.sDim(16,16,"h").height, width:"100%", tintColor:"#444", resizeMode:"contain" },
    separatorV:{ borderRightWidth: 1, borderRightColor:"#eee", borderStyle:"solid", height:"100%" },
    separatorH:{ borderBottomWidth: 1, borderBottomColor:"#eee", borderStyle:"solid", width:"100%" },
    modal:{ backgroundColor:"#FFF", width:"100%", borderRadius: 5, overflow:"hidden", maxWidth:350, elevation: 5, shadowColor: '#000', shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.5, },
    buttonModal:{height:50},
    buttonModalText:{textAlign: "left", paddingLeft:10, paddingRight:10, fontSize:16},
});

// EXTRAS //

// Base64
    var Base64 = {
        _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
        encode: function(input) {
            var output = "";
            var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
            var i = 0;
            input = Base64._utf8_encode(input);
            while (i < input.length) {
                chr1 = input.charCodeAt(i++);
                chr2 = input.charCodeAt(i++);
                chr3 = input.charCodeAt(i++);
                enc1 = chr1 >> 2;
                enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                enc4 = chr3 & 63;
                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                    enc4 = 64;
                }
                output = output + this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) + this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);
            }
            return output;
        },
        decode: function(input) {
            var output = "";
            var chr1, chr2, chr3;
            var enc1, enc2, enc3, enc4;
            var i = 0;
            input = input.replace(/[^A-Za-z0-9+/=]/g, "");
            while (i < input.length) {
                enc1 = this._keyStr.indexOf(input.charAt(i++));
                enc2 = this._keyStr.indexOf(input.charAt(i++));
                enc3 = this._keyStr.indexOf(input.charAt(i++));
                enc4 = this._keyStr.indexOf(input.charAt(i++));
                chr1 = (enc1 << 2) | (enc2 >> 4);
                chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                chr3 = ((enc3 & 3) << 6) | enc4;
                output = output + String.fromCharCode(chr1);
                if (enc3 !== 64) {
                    output = output + String.fromCharCode(chr2);
                }
                if (enc4 !== 64) {
                    output = output + String.fromCharCode(chr3);
                }
            }
            output = Base64._utf8_decode(output);
            return output;
        },
        _utf8_encode: function(string) {
            string = string.replace(/\r\n/g, "\n");
            var utftext = "";
            for (var n = 0; n < string.length; n++) {
                var c = string.charCodeAt(n);
                if (c < 128) {
                    utftext += String.fromCharCode(c);
                }
                else if ((c > 127) && (c < 2048)) {
                    utftext += String.fromCharCode((c >> 6) | 192);
                    utftext += String.fromCharCode((c & 63) | 128);
                }
                else {
                    utftext += String.fromCharCode((c >> 12) | 224);
                    utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                    utftext += String.fromCharCode((c & 63) | 128);
                }
            }
            return utftext;
        },
        _utf8_decode: function(utftext) {
            var string = "";
            var i = 0;
            var c, c2, c3 = 0;
            while (i < utftext.length) {
                c = utftext.charCodeAt(i);
                if (c < 128) {
                    string += String.fromCharCode(c);
                    i++;
                }
                else if ((c > 191) && (c < 224)) {
                    c2 = utftext.charCodeAt(i + 1);
                    string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                    i += 2;
                }
                else {
                    c2 = utftext.charCodeAt(i + 1);
                    c3 = utftext.charCodeAt(i + 2);
                    string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                    i += 3;
                }
            }
            return string;
        }
    };