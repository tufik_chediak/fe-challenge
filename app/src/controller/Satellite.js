import {log, cuppa} from "src/libs/cuppa/cuppa"

export const SATELLITE = "SATELLITE";
export default class Satellite{

    static setSatellite(data){  // {name:String}
        cuppa.setData(SATELLITE, {data:data})
    }
}